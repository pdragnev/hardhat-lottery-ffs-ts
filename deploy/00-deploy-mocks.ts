import { getNamedAccounts, deployments, network, ethers } from "hardhat"
import { DeployFunction } from "hardhat-deploy/types"
import { networkConfig, developmentChains } from "../helper-hardhat-config"

const BASE_FEE = ethers.utils.parseEther("0.25") //0.25 is the premium; cost 0.25 link per request
const GAS_PRICE_LINK = 1e9 //link per gas, calculated value based on the gas price of the chain.

const deployRaffle: DeployFunction = async ({ getNamedAccounts, deployments }) => {
    const { deploy, log } = deployments
    const { deployer } = await getNamedAccounts()
    const args = [BASE_FEE, GAS_PRICE_LINK]

    const chainId = network.config.chainId!

    if (developmentChains.includes(network.name)) {
        log("Local network detected! Deploying mocks...")

        await deploy("VRFCoordinatorV2Mock", {
            from: deployer,
            args: args,
            log: true,
            waitConfirmations: networkConfig?.[chainId]?.["blockConfirmations"] || 1,
        })
        log("Mocks deployed!")
        log("---------------------------------------")
    }
}

export default deployRaffle
deployRaffle.tags = ["all", "mocks"]
