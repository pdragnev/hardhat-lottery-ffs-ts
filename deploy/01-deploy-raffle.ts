import { BigNumber } from "ethers"
import { network, ethers } from "hardhat"
import { DeployFunction } from "hardhat-deploy/types"
import { networkConfig, developmentChains } from "../helper-hardhat-config"
import { verify } from "../utils/verify"
import "dotenv/config"
import { ContractReceipt } from "ethers"
import { VRFCoordinatorV2Mock } from "../typechain-types"

const VRF_SUB_FUND_AMOUNT = ethers.utils.parseEther("2")
const deployRaffle: DeployFunction = async ({ getNamedAccounts, deployments }) => {
    const { deploy } = deployments
    const { deployer } = await getNamedAccounts()
    const chainId = network.config.chainId!
    let vrfCoordinatorV2Address: string
    let vrfCoordinatorV2Mock: VRFCoordinatorV2Mock
    let subscriptionId

    if (developmentChains.includes(network.name)) {
        // create VRFV2 Subscription
        vrfCoordinatorV2Mock = await ethers.getContract("VRFCoordinatorV2Mock")
        vrfCoordinatorV2Address = vrfCoordinatorV2Mock.address
        const transactionResponse = await vrfCoordinatorV2Mock.createSubscription()
        const transactionReceipt: ContractReceipt = await transactionResponse.wait(1)
        subscriptionId = transactionReceipt.events?.find((value) => {
            return value.event! === "SubscriptionCreated"
        })?.args?.subId
        if (subscriptionId != undefined) {
            await vrfCoordinatorV2Mock.fundSubscription(subscriptionId, VRF_SUB_FUND_AMOUNT)
        } else {
            throw new Error("")
        }
    } else {
        vrfCoordinatorV2Address = networkConfig?.[chainId]?.vrfCoordinatorV2!
        subscriptionId = networkConfig?.[chainId]?.subscriptionId!
    }

    const entraceFee: BigNumber = networkConfig?.[chainId]?.entraceFee
    const gasLane: string = networkConfig?.[chainId]?.gasLane
    const callbackGasLimit: string = networkConfig?.[chainId]?.callbackGasLimit
    const raffleInterval: string = networkConfig?.[chainId]?.raffleInterval

    const args: any[] = [
        vrfCoordinatorV2Address,
        entraceFee,
        gasLane,
        subscriptionId,
        callbackGasLimit,
        raffleInterval,
    ]
    const raffleContract = await deploy("Raffle", {
        from: deployer,
        args: args,
        log: true,
        waitConfirmations: networkConfig?.[network.config.chainId!]?.blockConfirmations || 1,
    })

    if (developmentChains.includes(network.name)) {
        const vrfCoordinatorV2Mock = await ethers.getContract("VRFCoordinatorV2Mock")
        await vrfCoordinatorV2Mock.addConsumer(subscriptionId, raffleContract.address)
    }

    if (!developmentChains.includes(network.name) && process.env.ETHERSCAN_API_KEY) {
        verify(raffleContract.address, args)
    }
}

export default deployRaffle
deployRaffle.tags = ["all", "raffle"]
