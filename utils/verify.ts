import { run } from "hardhat"
//import { modules } from "web3"

export default async function verify(contractAdress: string, args: any[]) {
    console.log("Verifying contract..")
    try {
        await run("verify:verify", {
            address: contractAdress,
            constructorArguments: args,
        })
    } catch (error: any) {
        if (error.message.toLowerCase().includes("already verified")) {
            console.log("Already Verified!")
        }
    }
}
