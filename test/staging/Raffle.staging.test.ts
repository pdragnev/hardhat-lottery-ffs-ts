import { deployments, ethers, getNamedAccounts, network } from "hardhat"
import { assert, expect } from "chai"
import { Raffle, VRFCoordinatorV2Mock } from "../../typechain-types"
import { developmentChains, networkConfig } from "../../helper-hardhat-config"
import { BigNumber } from "ethers"

developmentChains.includes(network.name)
    ? describe.skip
    : describe("Raffle", () => {
          let deployer: string
          let entraceFee: BigNumber
          let raffleContract: Raffle

          beforeEach(async () => {
              deployer = (await getNamedAccounts()).deployer
              raffleContract = await ethers.getContract("Raffle", deployer)
              entraceFee = await raffleContract.getEntranceFee()
          })

          describe("Raffle Staging Tests", () => {
              it("works with Chainlink Automations & Chainlink VRF, picks winner, resets lottery and sends money", async () => {
                  const startingTimeStamp = await raffleContract.getLatestTaimeStamp()
                  const accounts = await ethers.getSigners()
                  const player = accounts[0]

                  //performUpkeep (Chainlink Automations)
                  //fulfillRandomWords (Chainlink VRF)
                  //Wait for Chainlink VRF to finish
                  //Setting up listener, below we will fire the event and listener will pick it up and resolve
                  //setting uplistener before entering raffle
                  console.log("Setting up Listener...")
                  await new Promise<void>(async (resolve, reject) => {
                      raffleContract.once("WinnerPicked", async () => {
                          console.log("Event fired!")
                          try {
                              const recentWinner = await raffleContract.getRecentWinner()
                              const winnerEndingBalance = await player.getBalance()
                              const raffleState = await raffleContract.getRaffleState()
                              const endingTimestamp = await raffleContract.getLatestTaimeStamp()

                              await expect(raffleContract.getPlayer(0)).to.be.reverted
                              assert.equal(recentWinner.toString(), player.address)
                              assert.equal(raffleState.toString(), "0")
                              assert(endingTimestamp > startingTimeStamp)
                              assert.equal(
                                  winnerEndingBalance.toString(),
                                  winnerStartingBalance.add(entraceFee).toString()
                              )
                              resolve()
                          } catch (error) {
                              console.log(error)
                              reject(error)
                          }
                      })

                      //Entering raffle
                      //this code won't complete until our listener has finished listening
                      console.log("Entering Raffle...")
                      const tx = await raffleContract.enterRaffle({
                          value: entraceFee,
                      })
                      await tx.wait(1)
                      console.log("Ok, time to wait...")
                      const winnerStartingBalance = await player.getBalance()
                  })
              })
          })
      })
