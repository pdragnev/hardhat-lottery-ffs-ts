import { deployments, ethers, getNamedAccounts, network } from "hardhat"
import { assert, expect } from "chai"
import { Raffle, VRFCoordinatorV2Mock } from "../../typechain-types"
import { developmentChains, networkConfig } from "../../helper-hardhat-config"
import { BigNumber } from "ethers"

!developmentChains.includes(network.name)
    ? describe.skip
    : describe("Raffle", () => {
          let deployer: string
          let entraceFee: BigNumber
          let raffleContract: Raffle
          let vrfCoordinatorV2Mock: VRFCoordinatorV2Mock
          const chainId = network.config.chainId!
          let raffleInterval: BigNumber

          beforeEach(async () => {
              deployer = (await getNamedAccounts()).deployer
              await deployments.fixture(["all"])
              raffleContract = await ethers.getContract("Raffle", deployer)
              vrfCoordinatorV2Mock = await ethers.getContract("VRFCoordinatorV2Mock", deployer)
              entraceFee = await raffleContract.getEntranceFee()
              raffleInterval = await raffleContract.getInterval()
          })

          describe("constructor", () => {
              it("initilizes the raffle correctly", async () => {
                  const raffleState = await raffleContract.getRaffleState()
                  assert.equal(raffleState.toString(), "0")
                  assert.equal(raffleInterval.toString(), networkConfig?.[chainId]?.raffleInterval!)
              })
          })

          describe("enterRaffle", () => {
              it("reverts when you don't pay enough", async () => {
                  await expect(raffleContract.enterRaffle()).to.be.revertedWith(
                      "Raffle__NotEnoughETHToEnter"
                  )
              })

              it("records players when they enter", async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  const playerFromContract = await raffleContract.getPlayer(0)
                  assert.equal(playerFromContract, deployer)
              })

              it("emits event on enter", async () => {
                  await expect(raffleContract.enterRaffle({ value: entraceFee })).to.emit(
                      raffleContract,
                      "RaffleEnter"
                  )
              })

              it("doesn't allow entrance when raffle is calculating", async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  //Mining block after interval + 1
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])
                  //We pretend to be Chainlink Keeper
                  await raffleContract.performUpkeep([])
                  await expect(
                      raffleContract.enterRaffle({ value: entraceFee })
                  ).to.be.revertedWith("Raffle__NotOpen")
              })
          })

          describe("checkUpkeep", () => {
              it("returns false if players haven't sent any ETH", async () => {
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])
                  const { upkeepNeeded } = await raffleContract.callStatic.checkUpkeep([])
                  assert.equal(upkeepNeeded, false)
              })
              it("returns false if raffle isn't open", async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])
                  await raffleContract.performUpkeep([])
                  const raffleState = await raffleContract.getRaffleState()
                  const { upkeepNeeded } = await raffleContract.callStatic.checkUpkeep([])
                  assert.equal(raffleState.toString(), "1")
                  assert.equal(upkeepNeeded, false)
              })

              it("returns false if not enough time passed", async () => {
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() - 1])
                  await network.provider.send("evm_mine", [])
                  const { upkeepNeeded } = await raffleContract.callStatic.checkUpkeep([])
                  assert.equal(upkeepNeeded, false)
              })

              it("returns true if time passed, has players, eth, is Open", async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])
                  const { upkeepNeeded } = await raffleContract.callStatic.checkUpkeep([])
                  assert.equal(upkeepNeeded, true)
              })
          })

          describe("performUpkeep", () => {
              it("it can only run if checkupkeep is true", async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])

                  const txResponse = await raffleContract.performUpkeep([])
                  assert(txResponse)
              })
              it("reverts if checkupkeep is false", async () => {
                  await expect(raffleContract.performUpkeep([])).to.be.revertedWith(
                      "Raffle__UpkeepNotNeeded"
                  )
              })

              it("updates raffle state, emits event, and calls vrf coordinator", async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])

                  const txResponse = await raffleContract.performUpkeep([])
                  const txReceipt = await txResponse.wait(1)

                  const raffleState = await raffleContract.getRaffleState()
                  const requestId: BigNumber = txReceipt.events?.find((value) => {
                      return value.event! === "RequestedRaffleWinner"
                  })?.args?.requestId

                  assert(requestId.toNumber() > 0)
                  assert.equal(raffleState, 1)
              })
          })

          describe("fulfillRandomWords", () => {
              beforeEach(async () => {
                  await raffleContract.enterRaffle({ value: entraceFee })
                  await network.provider.send("evm_increaseTime", [raffleInterval.toNumber() + 1])
                  await network.provider.send("evm_mine", [])
              })

              it("can only be called after performUpkeep", async () => {
                  await expect(
                      vrfCoordinatorV2Mock.fulfillRandomWords(0, raffleContract.address)
                  ).to.be.revertedWith("nonexistent request")
                  await expect(
                      vrfCoordinatorV2Mock.fulfillRandomWords(1, raffleContract.address)
                  ).to.be.revertedWith("nonexistent request")
              })
              it("picks winner, resets lottery and sends money", async () => {
                  const additionalEntrants = 3
                  const startingAccountIndex = 1
                  const accounts = await ethers.getSigners()
                  for (
                      let i = startingAccountIndex;
                      i < startingAccountIndex + additionalEntrants;
                      i++
                  ) {
                      const accountConnectedRaffle = raffleContract.connect(accounts[i])
                      await accountConnectedRaffle.enterRaffle({ value: entraceFee })
                  }
                  const startingTimeStamp = await raffleContract.getLatestTaimeStamp()

                  //performUpkeep (mock Chainlink Keepers)
                  //fulfillRandomWords (mock Chainlink VRF)
                  //Wait for Chainlink VRF to finish
                  //Setting up listener, below we will fire the event and listener will pick it up and resolve
                  await new Promise<any>(async (resolve, reject) => {
                      raffleContract.once("WinnerPicked", async () => {
                          console.log("Event fired!")
                          try {
                              const recentWinner = await raffleContract.getRecentWinner()
                              const winnerEndingBalance = await accounts[1].getBalance()
                              const raffleState = await raffleContract.getRaffleState()
                              const endingTimestamp = await raffleContract.getLatestTaimeStamp()
                              const numPlayers = await raffleContract.getNumberOfPlayers()

                              assert.equal(numPlayers.toString(), "0")
                              assert.equal(raffleState.toString(), "0")
                              assert(endingTimestamp > startingTimeStamp)
                              assert.equal(
                                  winnerEndingBalance.toString(),
                                  winnerStartingBalance
                                      .add(entraceFee.mul(additionalEntrants).add(entraceFee))
                                      .toString()
                              )
                          } catch (error) {
                              reject(error)
                          }
                          resolve("")
                      })

                      const winnerStartingBalance = await accounts[1].getBalance()
                      const tx = await raffleContract.performUpkeep([])
                      const txReceipt = await tx.wait(1)
                      const requestId = txReceipt.events?.find((value) => {
                          return value.event! === "RequestedRaffleWinner"
                      })?.args?.requestId
                      await vrfCoordinatorV2Mock.fulfillRandomWords(
                          requestId,
                          raffleContract.address
                      )
                  })
              })
          })
      })
