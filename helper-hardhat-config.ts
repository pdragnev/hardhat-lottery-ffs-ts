import { BigNumber } from "ethers"
import { ethers } from "hardhat"

export interface networkConfigItem {
    vrfCoordinatorV2?: string
    blockConfirmations?: number
    name: string
    entraceFee: BigNumber
    gasLane: string
    subscriptionId?: string
    callbackGasLimit: string
    raffleInterval: string
}

export interface networkConfigInfo {
    [chainId: number]: networkConfigItem
}

export const networkConfig: networkConfigInfo = {
    5: {
        name: "goerli",
        vrfCoordinatorV2: "0x2Ca8E0C643bDe4C2E08ab1fA0da3401AdAD7734D",
        blockConfirmations: 6,
        entraceFee: ethers.utils.parseEther("0.01"),
        gasLane: "0x79d3d8832d904592c0bf9818b621522c988bb8b0c05cdc3b15aea1b6e8db0c15",
        subscriptionId: "8180",
        callbackGasLimit: "500000",
        raffleInterval: "30",
    },
    31337: {
        name: "hardhat",
        entraceFee: ethers.utils.parseEther("0.01"),
        gasLane: "0x79d3d8832d904592c0bf9818b621522c988bb8b0c05cdc3b15aea1b6e8db0c15",
        callbackGasLimit: "500000",
        raffleInterval: "30",
    },
}

export const developmentChains = ["hardhat", "localhost"]
